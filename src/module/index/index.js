import 'bootstrap/dist/css/bootstrap.min.css'
import 'assets/xmgc-theme.css'
import Vue from 'vue'
import App from './App'

/* eslint-disable no-new */
new Vue({
  el: 'body',
  components: { App }
})
